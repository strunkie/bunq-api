import ApiAdapter from "../ApiAdapter";
import Session from "../Session";
import ApiEndpointInterface from "../Interfaces/ApiEndpointInterface";

export default class CertificatePinned implements ApiEndpointInterface {
    ApiAdapter: ApiAdapter;
    Session: Session;

    /**
     * @param {ApiAdapter} ApiAdapter
     */
    constructor(ApiAdapter: ApiAdapter) {
        this.ApiAdapter = ApiAdapter;
        this.Session = ApiAdapter.Session;
    }

    /**
     * @param {number} userId
     * @param {string} certificate
     * @returns {Promise<any>}
     */
    public async post(userId: number, certificate: string) {
        const limiter = this.ApiAdapter.RequestLimitFactory.create("/user", "POST");

        const response = await limiter.run(async () => this.ApiAdapter.post(`/v1/user/${userId}/certificate-pinned`, {
            certificate_chain: [{
                certificate: certificate
            }]
        }));

        return response.Response[0];
    }

    /**
     * @param {number} userId
     * @returns {Promise<any>}
     */
    public async list(userId: number) {
        const limiter = this.ApiAdapter.RequestLimitFactory.create("/user", "GET");

        const response = await limiter.run(async () => this.ApiAdapter.get(`/v1/user/${userId}/certificate-pinned`));

        return response.Response[0];
    }
}
