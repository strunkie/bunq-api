import ApiAdapter from "../ApiAdapter";
import Session from "../Session";
import ApiEndpointInterface from "../Interfaces/ApiEndpointInterface";
export default class CertificatePinned implements ApiEndpointInterface {
    ApiAdapter: ApiAdapter;
    Session: Session;
    /**
     * @param {ApiAdapter} ApiAdapter
     */
    constructor(ApiAdapter: ApiAdapter);
    /**
     * @param {number} userId
     * @param {string} certificate
     * @returns {Promise<any>}
     */
    post(userId: number, certificate: string): Promise<any>;
    /**
     * @param {number} userId
     * @returns {Promise<any>}
     */
    list(userId: number): Promise<any>;
}
