"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CertificatePinned {
    /**
     * @param {ApiAdapter} ApiAdapter
     */
    constructor(ApiAdapter) {
        this.ApiAdapter = ApiAdapter;
        this.Session = ApiAdapter.Session;
    }
    /**
     * @param {number} userId
     * @param {string} certificate
     * @returns {Promise<any>}
     */
    async post(userId, certificate) {
        const limiter = this.ApiAdapter.RequestLimitFactory.create("/user", "POST");
        const response = await limiter.run(async () => this.ApiAdapter.post(`/v1/user/${userId}/certificate-pinned`, {
            certificate_chain: [{
                    certificate: certificate
                }]
        }));
        return response.Response[0];
    }
    /**
     * @param {number} userId
     * @returns {Promise<any>}
     */
    async list(userId) {
        const limiter = this.ApiAdapter.RequestLimitFactory.create("/user", "GET");
        const response = await limiter.run(async () => this.ApiAdapter.get(`/v1/user/${userId}/certificate-pinned`));
        return response.Response[0];
    }
}
exports.default = CertificatePinned;
